#include "canvas.h"
#include <QPainter>

Canvas::Canvas(QWidget *parent) : QWidget(parent)
{

    setMinimumSize(canvasWidth,canvasHeight);
    Blob b1 = Blob(QColor(255,100,10),
                   QPoint(50, 80),
                   50);
    Blob b2 = Blob(QColor(10,10,120),
                   QPoint(80, 80),
                   50);
    Blob b3 = Blob(QColor(225,225,0),
                   QPoint(65,100),
                   50);
    Blob b4 = Blob(QColor(100,0,235),
                   QPoint(120,135),
                   40);

    Blob b5 = Blob(QColor(30,30,30),
                   QPoint(65,90),
                   40);

    std::vector<Blob> blobs= std::vector<Blob>();
    blobs.push_back(b1);
    blobs.push_back(b2);
    blobs.push_back(b3);
    blobs.push_back(b4);
    blobs.push_back(b5);
    dish = Dish(canvasWidth, canvasHeight, blobs);
    dish.paint();

}

void Canvas::paintEvent(QPaintEvent*) {
    dish.paint();
    QPainter p;
    p.begin(this);
    p.drawImage(QRect(0, 0,canvasWidth, canvasHeight), dish);
    p.end();


}
