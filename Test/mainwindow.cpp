#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "blob.h"
#include "dish.h"
#include "canvas.h"


MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    setCentralWidget(new Canvas(this));

}

MainWindow::~MainWindow()
{
    delete ui;
}
