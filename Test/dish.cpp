#include "dish.h"
#include <tuple>
#include <math.h>
#include <QVector3D>


float metaball(float d2, float b) {
    if (d2<=(b*b)) {
        //return b*b/d2;
        return (1 - 4*pow(d2,3)/(9*pow(b,6)) + 17*pow(d2,2)/(9*pow(b,4)) - 22*d2/(9*pow(b,2)));
    }
    else {
        return 0;
    }
    //return 10*exp(-d2/(7*b));
}


void Dish::pixelShader(QPoint p) {

    QRgb background = qRgb(200, 200, 200);
    colorTable().push_back(background);
    unsigned int n = blobs.size();
    QVector3D rgb = QVector3D(0,0,0);
    float sum = 0;
    for (unsigned int i =0; i < n; i++) {
        QVector3D c = QVector3D(blobs[i].getColor().redF(), blobs[i].getColor().greenF(), blobs[i].getColor().blueF());
        QPoint diff = p -  blobs[i].getCenter();
        float d2 = pow(diff.x(),2) +pow(diff.y(),2);
        rgb = rgb + c * metaball(d2,(float) blobs[i].getRadius());
        sum += metaball(d2,(float) blobs[i].getRadius());
    }

    if(sum >treshold) {
        rgb = rgb/sum;
        QRgb value = qRgb((int) 255*rgb.x(), (int) 255*rgb.y(), (int) 255*rgb.z());
        colorTable().push_back(value);
        setPixel(p, value);

    }
    else {
        //std::cout <<  <<std::endl;
        setPixel(p, background);
    }
}

void Dish::paint() {

    for (int y = 0; y < height(); y++) {
        for (int x = 0; x < width(); x++) {
            pixelShader(QPoint(x,y));
        }
    }
}
