#ifndef CANVAS_H
#define CANVAS_H
#include "dish.h"
#include <QWidget>

class Canvas : public QWidget
{
    Q_OBJECT

private :
    Dish dish;
    int canvasWidth =500;
    int canvasHeight =500;
public:
    explicit Canvas(QWidget *parent = 0);

signals:

public slots:
    void paintEvent(QPaintEvent*);
};

#endif // CANVAS_H
