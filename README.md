Barranquero Margot
Ndoko Arthur

# Projet IGR205 : 3D Palette

Using the palette color picker inspired by [1] for editing of 3D models.

"Playful Palette : An Interactive Parametric Color Mixer for Artists" Maria Shugrina, Jingwa Lu, Stephen Diverdi, ACM Transactions on Graphics, Vol. 36, No. 4, Article 61. July 2017